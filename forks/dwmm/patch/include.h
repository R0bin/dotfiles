/* Bar functionality */
#include "bar_indicators.h"
#include "bar_tagicons.h"

#include "bar_ltsymbol.h"
#include "bar_status.h"
#include "bar_status2d.h"
#include "bar_tags.h"
#include "bar_wintitle.h"
#include "bar_systray.h"

/* Other patches */
#include "cfacts.h"
#include "floatpos.h"
#include "focusmaster.h"
#include "movestack.h"
#include "pertag.h"
#include "restartsig.h"
#include "swallow.h"
#include "togglefullscreen.h"
#include "xrdb.h"
/* Layouts */
#include "layout_monocle.h"
#include "layout_tile.h"

