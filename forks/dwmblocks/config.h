//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/															/*Update Interval*/	/*Update Signal*/
	{"^c#cc6666^  ", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",		5,		0},
	{"^c#de935f^ ﬙  ", "echo `top -b -n1 | grep \"Cpu(s)\" | awk '{print $2 + $4}'`%",	5,		0},
	{"^c#8c9440^  ", "mpc current",													5,		0},
	{"^c#81a2be^  ", "echo $(cat /home/rob/.local/scripts/timeout)",					1,		0},
	{"^c#acadac^  ", "date '+%D %R'",													5,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ";
static unsigned int delimLen = 1;
