#!/bin/bash
cd /home/rob/doc/bks/

bk=$(fzf)

if [ "$bk" == '' ]; then
		exit
fi

zathura "$bk"
