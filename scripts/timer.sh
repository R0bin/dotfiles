#!/bin/bash
	time=$(cat time)

	echo $$ > /home/rob/.local/scripts/timepid

while :
	do
	time=$(cat /home/rob/.local/scripts/time)

	min=$(($time / 60))
	sec=$(($time % 60))

	if [ $min -lt 10 ]; then
			min=$(echo 0$min)
	fi

	if [ $sec -lt 10 ]; then
			sec=$(echo 0$sec)
	fi

	ttime=$(echo $min:$sec)
	sleep 1
	time=$(($time+1))
	echo $time > /home/rob/.local/scripts/time
	echo $ttime > /home/rob/.local/scripts/timeout
done
