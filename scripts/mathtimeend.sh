#!/bin/bash

if [ $(wc -l final | cut -d" " -f1) -gt 8 ]; then
		echo -e $(cat /home/rob/.local/scripts/final | sed '1,2d') >> /home/rob/doc/txt/final
		echo -e "Tiempo\t\tFecha\n=========================" > final
fi

echo -e "$(cat timeout) \t\t $(date '+%D')" >> final
