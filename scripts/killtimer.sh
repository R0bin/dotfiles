#!/bin/bash

if [[ $(cat /home/rob/.local/scripts/timepid) == '' ]]; then
		/bin/bash /home/rob/.local/scripts/timer.sh &
		$(play /home/rob/.local/scripts/pum.mp3)
		# $(mpc play)
else
		$(kill $(cat /home/rob/.local/scripts/timepid))
		echo '' > /home/rob/.local/scripts/timepid
		$(play /home/rob/.local/scripts/pum.mp3)
		# $(mpc pause)
fi
