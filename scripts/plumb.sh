#!/bin/bash

shopt -s expand_aliases
source ~/.bashrc

sel=$(xclip -o)

list="srx\n
wiki\n
trnt\n
"

plumb=$(echo -e $list | dmenu -p "$sel")
echo $plumb

if [ '$plumb' = 'srx\n' ]
	then
		$(qutebrowser "https://https://search.ononoki.org/search?q= $sel")
fi

if [ '$plumb' = 'wiki' ]
	 then
		$(qutebrowser "https://es.wikipedia.org/wiki/ $sel")
fi

if [ '$plumb' = 'trnt' ]
	then
transmission-remote -a "$sel"
fi

