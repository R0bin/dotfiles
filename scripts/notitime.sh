#!/bin/bash

if [ $(wc -l final | cut -d" " -f1) -gt 8 ]; then
		echo -e $(cat final | sed '1,2d') >> /home/rob/doc/txt/final
		echo -e "Tiempo\t\tFecha\n=========================" > final
fi

echo -e "$(cat /home/rob/.local/scripts/timeout) \t\t $(date '+%D')" >> final

$(notify-send -i "/home/rob/img/matess.png" "Estudiado en la semana:" "$(cat final)")
