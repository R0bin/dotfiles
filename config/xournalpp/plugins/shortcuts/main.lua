function initUi()
		app.registerUi({["menu"] = "lapiz", ["callback"] = "lapiz", ["accelerator"] = "z"});
		app.registerUi({["menu"] = "cortar", ["callback"] = "cortar", ["accelerator"] = "x"});
		app.registerUi({["menu"] = "borrar", ["callback"] = "borrar", ["accelerator"] = "c"});
		app.registerUi({["menu"] = "seleccionar", ["callback"] = "seleccionar", ["accelerator"] = "v"});
		app.registerUi({["menu"] = "regla", ["callback"] = "regla", ["accelerator"] = "q"});
		app.registerUi({["menu"] = "deshacer", ["callback"] = "deshacer", ["accelerator"] = "w"});
		app.registerUi({["menu"] = "rehacer", ["callback"] = "rehacer", ["accelerator"] = "e"});
		app.registerUi({["menu"] = "copiar", ["callback"] = "copiar", ["accelerator"] = "y"});
		app.registerUi({["menu"] = "blanco", ["callback"] = "blanco", ["accelerator"] = "a"});
		app.registerUi({["menu"] = "gris", ["callback"] = "gris", ["accelerator"] = "<Shift>a"});
		app.registerUi({["menu"] = "rojo", ["callback"] = "rojo", ["accelerator"] = "s"});
		app.registerUi({["menu"] = "verde", ["callback"] = "verde", ["accelerator"] = "d"});
		app.registerUi({["menu"] = "amarilllo", ["callback"] = "amarillo", ["accelerator"] = "f"});
		app.registerUi({["menu"] = "azul", ["callback"] = "azul", ["accelerator"] = "g"});
		app.registerUi({["menu"] = "violeta", ["callback"] = "violeta", ["accelerator"] = "<Shift>s"});
		app.registerUi({["menu"] = "cyan", ["callback"] = "cyan", ["accelerator"] = "<Shift>g"});

end

function lapiz()
	app.uiAction({["action"]="ACTION_TOOL_PEN"}) 
end

function cortar()
	app.uiAction({["action"]="ACTION_CUT"})
end

function borrar()
	app.uiAction({["action"]="ACTION_TOOL_ERASER"})
end

function seleccionar()
	app.uiAction({["action"]="ACTION_SELECT_RECT"})
end

function regla()
	app.uiAction({["action"]="ACTION_RULER"})
end

function deshacer()
	app.uiAction({["action"]="ACTION_UNDO"})
end

function rehacer()
	app.uiAction({["action"]="ACTION_REDO"})
end

function copiar()
	app.uiAction({["action"]="ACTION_COPY"})
end

function pegar()
	app.uiAction({["action"]="ACTION_PASTE"})
end


function blanco()
	app.changeToolColor({["color"] = 0xacadac})
end

function gris()
	app.changeToolColor({["color"] = 0xb5bd68})
end

function rojo()
	app.changeToolColor({["color"] = 0xcc6666})
end

function verde()
	app.changeToolColor({["color"] = 0x8c9440})
end

function amarillo()
	app.changeToolColor({["color"] = 0xde935f})
end

function azul()
	app.changeToolColor({["color"] = 0x5f819d})
end

function cyan()
	app.changeToolColor({["color"] = 0x85678f})
end

function violeta()
	app.changeToolColor({["color"] = 0x5e8d87})
end
